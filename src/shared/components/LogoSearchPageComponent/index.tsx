import LogoComputer from '../../assets/img/undraw_handcrafts_computer 1.svg'
import './style.css'

const LogoSearchPage: React.FC = ()    => {
    return(
        <div className="boxLogoSearchPage">
            <img src = {LogoComputer} alt="Imagem de um computador" className="ComputerLogoSearchPage"/>
            <h2 className="TitleLogoSearchPage">ALon</h2>
        </div>
           
    )
}

export default LogoSearchPage