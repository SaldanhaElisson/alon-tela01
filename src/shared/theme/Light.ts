import {createTheme} from '@mui/material'

interface PaletteColor {
  light?: string;
  main: string;
  dark?: string;
  contrastText?: string;
}


export const LightThemer = createTheme({
    palette:{
        primary:{
            light: '#164212',
            main: '#164212',
            dark: '#164212',
            contrastText: '#ffff'
        }
    }
})