import { Button, Container, InputAdornment, TextField} from "@mui/material"
import searchImg  from "../../shared/assets/img/Vector.svg"





import InputSearch from "../../shared/components/inputSearch"
import LogoHomePage from "../../shared/components/LogoComponent"

const HomePage = () => {
    return (
        <>
            <Container sx={{
                maxWidth:'sm',
                height: '100vh',

                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
                
            }}>
            
                <LogoHomePage/>

                <TextField 
                id="outlined-basic" 
                label="Email, Matricula ou N° Protocolo.." 
                variant="outlined" 
                fullWidth 

                InputProps={{
                    endAdornment:(
                        <InputAdornment 
                        position="end"
                        > 
                            <img src={searchImg} alt="Lupa" />
                        </InputAdornment>
                    )
                }}

                sx={{
                    marginTop: '5rem'
                }}
                />
            
                <Button variant="outlined"
                    sx ={{
                        width: '8.75rem',
                        height: '2.5rem',

                        fontSize: '0.738rem',


                      
                        border: '2px solid',
                        marginTop: '3.75rem'
                    }}
                > 
                
                Pesquisar
                
                </Button>


                <Button 
                variant="contained"
                sx={{
                   
                    fontSize: '0.738rem',
                    
                    width: '8.75rem',
                    height: '2.5rem',

                    marginTop: '2.5rem'
                    
                }}
                
                >
                    Novo Protocolo
                </Button>

            </Container>

        </>
    )
}

export default HomePage
