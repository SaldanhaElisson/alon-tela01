import { ThemeProvider } from '@mui/material'
import { LightThemer } from './shared/theme'


import HomePage from './pages/homePage'

import './shared/assets/style/global-styles.css'

function App(): JSX.Element {

  return (

    <ThemeProvider theme={LightThemer}>

      <HomePage />   
    </ThemeProvider>
  )
}

export default App
